// Wimpy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ProcessWrapper.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[]) {
	ProcessWrapper* wrapper=new ProcessWrapper("ngrok.exe",9008);
	wrapper->start();
	if(wrapper->hasError()) {
		cout << "Wrapper has error" << endl;
		delete wrapper;
		return 1;
	}
	unsigned char* ip=wrapper->getIP();
	unsigned short port=wrapper->getPort();
	unsigned int pid=wrapper->getPID();
	delete wrapper;
	cout << "Connected to " << ip<< " on port " << port << " using process ID " << pid << endl;
	return 0;
}

