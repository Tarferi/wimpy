#include "StdAfx.h"
#include "ProcessWrapper.h"

using namespace std;

ProcessWrapper::ProcessWrapper(char* fileName, unsigned short targetPort) {
	this->fileName=fileName;
	this->targetPort=targetPort;
	this->finalIP=NULL;
	this->finalPort=0;
	string h;
	h+=fileName;
	this->error=false;
	h+=" tcp ";
	char numstr[21];
	_itoa_s(targetPort, numstr, 10);
	h+=numstr;
	const char* res=h.c_str();
	int len=(strlen(res)+1);
	this->exeCommand=(TCHAR*)malloc(len*sizeof(TCHAR));
	if(!this->exeCommand){
		this->error=true;
		printf( "Malloc failed (%d).\n", GetLastError() );
		return;
	}

	for(int i=0;i<len;i++) {
		this->exeCommand[i]=res[i];
	}
	this->exeCommand[len-1]=0;
}

unsigned char* ProcessWrapper::getIP() {
	return this->finalIP;
}

unsigned short ProcessWrapper::getPort(){
	return this->finalPort;
}


ProcessWrapper::~ProcessWrapper(void){
	if (this->exeCommand) {
		free(this->exeCommand);
	}
}

unsigned int ProcessWrapper::getPID() {
	return this->process.dwProcessId;
}

void ProcessWrapper::start() {
	this->spawnProcess();
}

bool ProcessWrapper::hasError() {
	return this->error;
}

const int dataPointer=0x11515EA0;

char* ProcessWrapper::getIPFromHostname(char* hostName) {
	WSADATA wsaData;
	int iResult;
	DWORD dwRetval;
	int i = 1;
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;
	struct sockaddr_in  *sockaddr_ipv4;
	DWORD ipbufferlength = 46;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		this->error=true;
		return NULL;
	}
	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	dwRetval = getaddrinfo(hostName,NULL, &hints, &result);
	if ( dwRetval != 0 ) {
		WSACleanup();
		this->error=true;
		return NULL;
	}
	char* qresult=NULL;
	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {
		i++;
		if(ptr->ai_family == AF_INET) {
			sockaddr_ipv4 = (struct sockaddr_in *) ptr->ai_addr;
			qresult=inet_ntoa(sockaddr_ipv4->sin_addr);
		}
	}
	freeaddrinfo(result);
	WSACleanup();
	return qresult;
}


bool ProcessWrapper::extractData(char* array, int maxLength) {
	string h="";
	bool found=false;
	char* pattern1="\"URL\":\"";
	char* pattern2="\"";
	char* begin=strstr(array,pattern1);
	if(begin != NULL) {
		begin+=strlen(pattern1);
		char* end=strstr(begin,pattern2);
		if(end!=NULL) {
			*end=0;
			int length=(int)end-(int)begin;
			char* delim=strstr(begin,"://");
			if(delim != NULL) {
				delim+=3;
				char* ndelim=strstr(delim,":");
				if(ndelim != NULL) {
					*ndelim=0;
					char* port=ndelim+1;
					char* ip=delim;
					ip=this->getIPFromHostname(ip);
					if(ip != NULL) {
						this->finalIP=(unsigned char*)malloc((strlen(ip)+1)*sizeof(unsigned char));
						if(!this->finalIP) {
							this->error=true;
						} else {
							memcpy(this->finalIP,ip,strlen(ip)+1);
							this->finalPort=atoi(port);
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

void ProcessWrapper::spawnProcess() {
	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	si.cb=sizeof(si);
	if (CreateProcess(NULL, (TCHAR*) this->exeCommand, NULL, NULL, FALSE, CREATE_NO_WINDOW|CREATE_SUSPENDED, NULL, NULL, &si, &pi)) {
		this->process=pi;
		ResumeThread(pi.hThread);
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE,pi.dwProcessId);
		if(!hProcess) {
			this->error=true;
			return;
		}
		char tmpArray[512];
		unsigned char lastArr[4];
		int lastPointer=0;
		while(true) {
			Sleep(200);
			DWORD dwReadBytes;
			ReadProcessMemory(hProcess, (void*)dataPointer, &lastArr,sizeof(lastArr)*sizeof(char),&dwReadBytes);
			if(dwReadBytes == 4) {
				lastPointer=lastArr[0]|(lastArr[1]<<8)|(lastArr[2]<<16)|(lastArr[3]<<24);
			} else {
				this->error=true;
				break;
			}
			if(lastPointer!=0) {
				DWORD dwReadBytes;
				ReadProcessMemory(hProcess, (void*)lastPointer, &tmpArray,sizeof(tmpArray)*sizeof(char),&dwReadBytes);

				if(dwReadBytes > 0) {
					if(tmpArray[0] == '{') {
						if(this->extractData(tmpArray,sizeof(tmpArray))) {
							break;
						}
					}
				}
			}
		}
		CloseHandle(hProcess);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	} else {
		this->error=true;
	}
}