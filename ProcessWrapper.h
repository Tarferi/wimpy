#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <tchar.h>
#include <string>
#include <ws2tcpip.h>
#include <winsock2.h>
#pragma comment (lib, "Ws2_32.lib")

class ProcessWrapper {
public:
	ProcessWrapper(char* fileName, unsigned short targetPort);
	~ProcessWrapper(void);

	void start();

	unsigned char* getIP();

	unsigned short getPort();

	unsigned int getPID();

	bool hasError();

private:
	void spawnProcess();
	bool extractData(char* array, int maxLength);
	char* getIPFromHostname(char* hostName);

	bool error;
	char* fileName;
	TCHAR* exeCommand;
	unsigned short targetPort;

	unsigned char* finalIP;
	unsigned short finalPort;

	PROCESS_INFORMATION process;
};

