# Wimpy #

Throwaway for somebody else to take over.

Basic example of how ngrok can be used (wrapped). The startup takes seconds to minute, depending on upstream frequency. Also, the result is not guaranteed, and timer should be used to terminate the process, if there are not any valid data for a longer period of time.